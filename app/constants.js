//action types
export const INIT_APP = 'INIT_APP';
export const FETCH_QUESTIONS = 'fetch_questions';
export const FETCH_QUESTION = 'fetch_question';
export const DELETE_QUESTION = 'delete_question';
export const CREATE_QUESTION = 'create_question';

//screenState
export const LOGIN_SCREEN = 'LOGIN_SCREEN';
