import { combineReducers } from 'redux';
import * as types from '../constants';
import QuestionsReducer from './reducer_questions';

const appInit = (state = false, action) => {
    switch (action.type) {
        case types.INIT_APP:
            return true;
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    init: appInit,
    questions: QuestionsReducer
});

export default rootReducer;
