import _ from 'lodash';
import * as types from '../constants';

export default function(state = {}, action) {
    switch (action.type) {
        case types.DELETE_QUESTION:
            return _.omit(state, action.payload);
        case types.FETCH_QUESTION:
            return { ...state, [action.payload.data.id]: action.payload.data };
        case types.FETCH_QUESTIONS:
            return _.mapKeys(action.payload.data, 'id');
        default:
            return state;
    }
}
