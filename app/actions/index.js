import * as types from '../constants';
import axios from 'axios';

const ROOT_URL = 'http://localhost:8000/api';

export function initApp() {
    return {
        type: types.INIT_APP
    };
}

export function fetchQuestions() {
    const request = axios.get(`${ROOT_URL}/questions`);

    return {
        type: types.FETCH_QUESTIONS,
        payload: request
    };
}

export function fetchQuestion(id) {
    const request = axios.get(`${ROOT_URL}/questions/${id}`);

    return {
        type: types.FETCH_QUESTION,
        payload: request
    };
}

export function deleteQuestion(id, callback) {
    const request = axios
        .delete(`${ROOT_URL}/questions/${id}`)
        .then(() => callback());

    return {
        type: types.DELETE_QUESTION,
        payload: request
    };
}

export function createQuestion(values, callback) {
    const request = axios
        .post(`${ROOT_URL}/questions`, values)
        .then(() => callback());

    return {
        type: types.CREATE_QUESTION,
        payload: request
    };
}
