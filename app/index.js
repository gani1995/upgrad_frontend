import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Root from './containers/Root';
import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './saga';

const sagaMiddleware = createSagaMiddleware();
function configureStore(initialState) {
    const composeEnhancer =
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const enhancer = composeEnhancer(applyMiddleware(sagaMiddleware));
    const store = createStore(rootReducer, initialState, enhancer);
    return store;
}

const store = configureStore();
let sagaTask = sagaMiddleware.run(function*() {
    yield rootSaga();
});

const render = () => {
    ReactDOM.render(
        <AppContainer>
            <Root store={store} />
        </AppContainer>,
        document.getElementById('root')
    );
};

render();

if (module.hot) {
    module.hot.accept('./reducers', () => {
        const nextRootReducer = require('./reducers').default;
        store.replaceReducer(nextRootReducer);
    });
    render();
    module.hot.accept('./saga', () => {
        const getNewSagas = require('./saga').default;
        sagaTask.cancel();
        sagaTask.done.then(() => {
            sagaTask = sagaMiddleware.run(function* replacedSaga() {
                yield getNewSagas();
            });
        });
    });
}

if (module.hot) {
    module.hot.accept('./containers/Root', () => {
        const NewRoot = require('./containers/Root').default;
        render(
            <AppContainer>
                <NewRoot store={store} />
            </AppContainer>,
            document.getElementById('root')
        );
    });
}
