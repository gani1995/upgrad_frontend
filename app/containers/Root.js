import PropTypes from 'prop-types';
import React from 'react';
import { Provider } from 'react-redux';
import Main from '../components/Main';

export default function Root({ store }) {
    return (
        <Provider store={store}>
            <Main />
        </Provider>
    );
}

Root.propTypes = {
    store: PropTypes.object.isRequired
};
