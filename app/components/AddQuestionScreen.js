import React, { Component } from 'react';
import { createQuestion } from '../actions';
import { connect } from 'react-redux';
//import styles from '../styles/add-question-screen.scss';

class AddQuestionScreen extends Component {
    handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({
            [name]: value
        });
    }

    onSubmit(values) {
        this.props.createQuestion(values, () => {
            this.props.history.push('/');
        });
        this.setState({
            savedQuestion: values
        });
    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <div>
                <h2>Add Question</h2>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <div className="form-group">
                        <label htmlFor="newQuestionType">
                            What type of question you want to create:
                        </label>
                        <br />
                        <input
                            type="radio"
                            name="newQuestionType"
                            value="mcq"
                            onChange={this.handleChange}
                        />{' '}
                        Multiple Choice Question<br />
                        <input
                            type="radio"
                            name="newQuestionType"
                            value="submissionType"
                            onChange={this.handleChange}
                        />{' '}
                        Submission Type Question<br />
                        <input
                            type="radio"
                            name="newQuestionType"
                            value="passageType"
                            onChange={this.handleChange}
                        />{' '}
                        Passage Type Question
                    </div>
                    <div className="form-group">
                        <label htmlFor="newQuestionTitle">
                            Question Title:
                        </label>
                        <input
                            id="newQuestionId"
                            className="form-control"
                            name="newQuestionTitle"
                            type="text"
                            value={this.state.newQuestionTitle}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="newQuestionDescription">
                            Question Description:
                        </label>
                        <input
                            id="newQuestionId"
                            className="form-control"
                            name="newQuestionDescription"
                            type="text"
                            value={this.state.newQuestionDescription}
                            onChange={this.handleChange}
                        />
                    </div>
                    <AddQuestionFormScreen
                        newQuestionType={this.state.newQuestionType}
                        newCorrectAnswer={this.state.newCorrectAnswer}
                        newIncorrectAnswer1={this.state.newIncorrectAnswer1}
                        newIncorrectAnswer2={this.state.newIncorrectAnswer2}
                        newIncorrectAnswer3={this.state.newIncorrectAnswer3}
                        handleChange={this.handleChange}
                    />
                    <div className="form-group">
                        <label htmlFor="newQuestionInstructions">
                            Question Instructions:
                        </label>
                        <input
                            id="newQuestionId"
                            className="form-control"
                            name="newQuestionInstruction"
                            type="text"
                            value={this.state.newQuestionInstruction}
                            onChange={this.handleChange}
                        />
                    </div>
                    <button
                        onClick={this.handleSubmit}
                        type="submit"
                        className="btn btn-default"
                    >
                        Author
                    </button>
                </form>
            </div>
        );
    }
}

export default connect(null, { createQuestion })(AddQuestionScreen);
