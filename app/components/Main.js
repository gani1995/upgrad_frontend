import React from 'react';

import UpgradBody from './UpgradBody';

import main from '../styles/main.scss';

class Main extends React.Component {
    render() {
        return (
            <div className={main.upgrad}>
                <div className={main.header}>
                    <img
                        className={main.upgradImage}
                        src={require('../assets/upgrad.png')}
                        alt="upgrad"
                    />
                </div>
                <UpgradBody />
            </div>
        );
    }
}

export default Main;
