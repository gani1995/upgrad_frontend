import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import LoginScreen from './LoginScreen';

import { LOGIN_SCREEN } from '../constants';

class UpgradBody extends Component {
    static propTypes = {
        screenState: PropTypes.string.isRequired
    };

    renderScreen = () => {
        switch (this.props.screenState) {
            case LOGIN_SCREEN:
                return <LoginScreen />;
            default:
                return <LoginScreen />;
        }
    };

    render() {
        return this.renderScreen();
    }
}

const mapStateToProps = state => ({
    state: state.screenState
});

export default connect(mapStateToProps)(UpgradBody);
