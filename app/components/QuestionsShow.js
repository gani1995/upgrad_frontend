import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchQuestion, deleteQuestion } from '../actions';

class QuestionsShow extends Component {
    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.fetchQuestion(id);
    }

    onDeleteClick() {
        const { id } = this.props.match.params;

        this.props.deleteQuestion(id, () => {
            this.props.history.push('/');
        });
    }

    render() {
        const { question } = this.props;

        if (!question) {
            return <div>Loading...</div>;
        }

        return (
            <div>
                <Link to="/">Back To Index</Link>
                <button
                    className="btn btn-danger pull-xs-right"
                    onClick={this.onDeleteClick.bind(this)}
                >
                    Delete Question
                </button>
                <h3>{question.title}</h3>
                <h6>{question.description}</h6>
                <p>{question.instructions}</p>
            </div>
        );
    }
}

function mapStateToProps({ questions }, ownProps) {
    return { question: questions[ownProps.match.params.id] };
}

export default connect(mapStateToProps, { fetchQuestion, deleteQuestion })(
    QuestionsShow
);
