import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchQuestions } from '../actions';

class QuestionsIndex extends Component {
    componentDidMount() {
        this.props.fetchQuestions();
    }

    renderQuestions() {
        return _.map(this.props.questions, question => {
            return (
                <li className="list-group-item" key={question.id}>
                    <Link to={`/questions/${question.id}`}>
                        {question.title}
                    </Link>
                </li>
            );
        });
    }

    render() {
        return (
            <div>
                <div className="text-xs-right">
                    <Link className="btn btn-primary" to="/questions/new">
                        Author New Question
                    </Link>
                </div>
                <h3>Question List</h3>
                <ul className="list-group">{this.renderQuestions()}</ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { questions: state.questions };
}

export default connect(mapStateToProps, { fetchQuestions })(QuestionsIndex);
