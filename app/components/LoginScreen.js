import React, { Component } from 'react';
import styles from '../styles/login-screen.scss';

class LoginScreen extends Component {
    render() {
        return (
            <div className={styles.loginScreen}>
                <div className={styles.loginScreenModal}>
                    <div className={styles.loginScreenheader}>
                        Please Select Login
                    </div>
                    <div className={styles.loginScreenchoice}>
                        <div className={styles.loginScreenContent}>
                            I am a ..
                        </div>
                        <label htmlFor="login-student">
                            <input
                                type="radio"
                                name="type"
                                value="student"
                                id="login-student"
                            />Student
                        </label>
                        <label htmlFor="login-teacher">
                            <input
                                type="radio"
                                name="type"
                                value="teacher"
                                id="login-teacher"
                            />Teacher
                        </label>
                    </div>
                    <div className={styles.loginScreenbutton}>
                        Please login to continue...
                    </div>
                </div>
            </div>
        );
    }
}

export default LoginScreen;
